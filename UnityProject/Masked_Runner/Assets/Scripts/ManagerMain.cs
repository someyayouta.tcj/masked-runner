﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;

public class ManagerMain : MonoBehaviour
{
    [SerializeField] private Text rankingField = default;
    [SerializeField] private Text inputName = default;
    [SerializeField] private Button RankingList = default;
    [HideInInspector] private int count;

    private List<MemberData> _memberList;

    public void OnClickClearDisplay()
    {
        //rankingField.text = " ";
    }

    public void OnClickGetJsonFromWebRequest()
    {
        GetJsonFromWebRequest();
    }

    public void OnRankingButtonList()
    {
        string sStrOutput = "";

        if (null == _memberList)
        {
            sStrOutput = "no list !";
        }
        else
        {
            sStrOutput = "Ranking \n";
            foreach (MemberData memberOne in _memberList)
            {
                count++;
                sStrOutput += $"{count}位 {memberOne.Name} {memberOne.Score}\n";
            }
        }

        rankingField.text = sStrOutput;
        RankingList.gameObject.SetActive(false);

    }

    private void GetJsonFromWebRequest()
    {
        StartCoroutine(
            DownloadJson(
                CallbackWebRequestSuccess, // APIコールが成功した際に呼ばれる関数を指定
                CallbackWebRequestFailed // APIコールが失敗した際に呼ばれる関数を指定
            )
        );
    }

    private void CallbackWebRequestSuccess(string response)
    {
        _memberList = MemberDataModel.DeserializeFromJson(response);

        //memberList ここにデコードされたメンバーリストが格納される。

        //rankingField.text = "Success!";
    }

    private void CallbackWebRequestFailed()
    {
        // jsonデータ取得に失敗した
        //rankingField.text = "WebRequest Failed";
    }

    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/maskedrunerapi/finalproject/getRanking");
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            // リクエスト成功の場合
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }

    public void OnClickSetRanking()
    {
        //rankingField.text = "wait...";
        SetJsonFromWWW();
    }

    private void SetJsonFromWWW()
    {
        string sTgtURL = "http://localhost/maskedrunerapi/finalproject/setRanking";

        string name = inputName.text;
        int score = ScoreManager.score;

        StartCoroutine(SetRanking(sTgtURL, name, score, WebRequestSuccess, CallbackWebRequestFailed));
    }

    private IEnumerator SetRanking(string url, string name, int score, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("score", score);

        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);

        webRequest.timeout = 5;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            Debug.Log($"Success:{webRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    private void WebRequestSuccess(string response)
    {
        //rankingField.text = response;
    }
}