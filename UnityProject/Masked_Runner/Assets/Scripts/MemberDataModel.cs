﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON; // Json

/// <summary>
/// Json response manager.
/// </summary>
public class MemberDataModel
{

    public static List<MemberData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<MemberData>();

        IList jsonList = (IList)Json.Deserialize(sStrJson);

        foreach (IDictionary jsonOne in jsonList)
        {

            var tmp = new MemberData();

            if (jsonOne.Contains("Name"))
            {
                tmp.Name = (string)jsonOne["Name"];
            }

            if (jsonOne.Contains("Score"))
            {
                tmp.Score = (long)jsonOne["Score"];
            }

            ret.Add(tmp);
        }

        return ret;
    }
}