﻿public class MemberData
{
    public string Name { get; set; }
    public long Score { get; set; }

    public MemberData()
    {
        Name = "";
        Score = 0;
        
    }
}
