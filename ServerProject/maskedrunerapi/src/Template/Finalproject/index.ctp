<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finalproject[]|\Cake\Collection\CollectionInterface $finalproject
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Finalproject'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="finalproject index large-9 medium-8 columns content">
    <h3><?= __('Finalproject') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Score') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($finalproject as $finalproject): ?>
            <tr>
                <td><?= $this->Number->format($finalproject->Id) ?></td>
                <td><?= h($finalproject->Name) ?></td>
                <td><?= $this->Number->format($finalproject->Score) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $finalproject->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $finalproject->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $finalproject->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $finalproject->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
