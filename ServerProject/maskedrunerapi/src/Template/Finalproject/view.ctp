<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finalproject $finalproject
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Finalproject'), ['action' => 'edit', $finalproject->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Finalproject'), ['action' => 'delete', $finalproject->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $finalproject->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Finalproject'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Finalproject'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="finalproject view large-9 medium-8 columns content">
    <h3><?= h($finalproject->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($finalproject->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($finalproject->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($finalproject->Score) ?></td>
        </tr>
    </table>
</div>
