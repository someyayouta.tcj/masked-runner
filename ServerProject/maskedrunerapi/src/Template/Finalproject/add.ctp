<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Finalproject $finalproject
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Finalproject'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="finalproject form large-9 medium-8 columns content">
    <?= $this->Form->create($finalproject) ?>
    <fieldset>
        <legend><?= __('Add Finalproject') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Score');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
