<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Finalproject Controller
 *
 * @property \App\Model\Table\FinalprojectTable $Finalproject
 *
 * @method \App\Model\Entity\Finalproject[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinalprojectController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $finalproject = $this->paginate($this->Finalproject);

        $this->set(compact('finalproject'));
    }

    /**
     * View method
     *
     * @param string|null $id Finalproject id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finalproject = $this->Finalproject->get($id, [
            'contain' => []
        ]);

        $this->set('finalproject', $finalproject);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finalproject = $this->Finalproject->newEntity();
        if ($this->request->is('post')) {
            $finalproject = $this->Finalproject->patchEntity($finalproject, $this->request->getData());
            if ($this->Finalproject->save($finalproject)) {
                $this->Flash->success(__('The finalproject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finalproject could not be saved. Please, try again.'));
        }
        $this->set(compact('finalproject'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Finalproject id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finalproject = $this->Finalproject->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finalproject = $this->Finalproject->patchEntity($finalproject, $this->request->getData());
            if ($this->Finalproject->save($finalproject)) {
                $this->Flash->success(__('The finalproject has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The finalproject could not be saved. Please, try again.'));
        }
        $this->set(compact('finalproject'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Finalproject id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $finalproject = $this->Finalproject->get($id);
        if ($this->Finalproject->delete($finalproject)) {
            $this->Flash->success(__('The finalproject has been deleted.'));
        } else {
            $this->Flash->error(__('The finalproject could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getRanking(){
        error_log("getRanking()");
        $this->autoRender = false;

        $query = $this->Finalproject->find('all');

        $query->order(['score' => 'DESC']); 
        $query->limit(10);  

        $json_array = json_encode($query);

        echo $json_array;
    }

    public function setRanking(){
        error_log("setRanking");

        $this->autoRender = false;

        $name = "";
        if(isset($this->request->data['name'])){
            $name = $this->request->data['name'];
            error_log($name);
        }

        $score = "";
        if(isset($this->request->data['score'])){
            $score = $this->request->data['score'];
            error_log($score);
        }

        $data   = array ( 'Name' => $name, 'Score' => $score, 'Date' => date('Y/m/d H:i:s') ); //timestamp型
    
        $finalproject = $this->Finalproject->newEntity();
        $finalproject = $this->Finalproject->patchEntity($finalproject, $data);

        if ($this->Finalproject->save($finalproject)) {
            //追加成功
            echo "success!"; //success!
        }else{
            //追加失敗
            echo "failed!"; //failed!
        }
    }
}
