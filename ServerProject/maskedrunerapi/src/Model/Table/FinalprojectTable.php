<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Finalproject Model
 *
 * @method \App\Model\Entity\Finalproject get($primaryKey, $options = [])
 * @method \App\Model\Entity\Finalproject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Finalproject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Finalproject|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Finalproject saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Finalproject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Finalproject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Finalproject findOrCreate($search, callable $callback = null, $options = [])
 */
class FinalprojectTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('finalproject');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->integer('Score')
            ->requirePresence('Score', 'create')
            ->notEmptyString('Score');

        return $validator;
    }
}
