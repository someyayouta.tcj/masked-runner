<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinalprojectTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinalprojectTable Test Case
 */
class FinalprojectTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FinalprojectTable
     */
    public $Finalproject;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Finalproject'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Finalproject') ? [] : ['className' => FinalprojectTable::class];
        $this->Finalproject = TableRegistry::getTableLocator()->get('Finalproject', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Finalproject);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
