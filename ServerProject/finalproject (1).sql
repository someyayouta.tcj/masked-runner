-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techstadium_kadai`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `finalproject`
--

CREATE TABLE `finalproject` (
  `Id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Score` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `finalproject`
--

INSERT INTO `finalproject` (`Id`, `Name`, `Score`) VALUES
(35, 'JJ', 2240),
(36, 'Valley', 2430),
(37, 'LL', 1005),
(38, 'aaaaaa', 900),
(39, 'iiiiiiii', 2360),
(40, 'yuya', 2920),
(41, '???', 2980),
(42, 'GG', 3735),
(43, '4444', 4505),
(44, 'z', 2380),
(45, '555', 915),
(46, 'GGGG', 4360),
(47, 'NiceRun', 2190),
(48, 'DDD', 5235),
(49, 'JJ', 4200),
(50, '2525', 5825),
(51, 'yuji', 2155),
(52, 'yuji', 6040),
(53, 'nice', 5905),
(54, 'ttt', 970),
(55, 'TTT', 6610),
(56, '444', 6620),
(57, 'GG', 6295),
(58, 'SY', 6520),
(59, 'NO', 6485),
(60, 'GG', 6270);

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `finalproject`
--
ALTER TABLE `finalproject`
  ADD PRIMARY KEY (`Id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `finalproject`
--
ALTER TABLE `finalproject`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
